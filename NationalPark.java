import java.util.Scanner;
public class NationalPark
{
	public static void main (String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Samoyed[] samoyedFamily = new Samoyed[4];
		
		for (int i = 0; i < samoyedFamily.length; i++)
		{
			System.out.println("For Samoyed #" + (i+1) + ", answer in this order: age, gender, yes/no if the dog is hypoallergenic?");			
			int age = scan.nextInt();
			String gender = scan.next();
			boolean hypoallergenic = scan.next().charAt(0) == 'y';
			samoyedFamily[i] = new Samoyed(age, gender, hypoallergenic);
			
			scan.nextLine();	
			
			System.out.println();
		}
		
		System.out.println("The last dog's info is in the order of age then gender:");
		System.out.println(samoyedFamily[3].getAge() + ", " + samoyedFamily[3].getGender());
		System.out.println("Update the last dog's info in this order: age, then gender.");
		samoyedFamily[3].setAge(scan.nextInt());
		samoyedFamily[3].setGender(scan.next());
		System.out.println("The last dog's UPDATED info is in the order of age then gender:");
		System.out.println(samoyedFamily[3].getAge() + ", " + samoyedFamily[3].getGender());
		
		System.out.println();
		
		System.out.print("The last samoyed is ");
		System.out.println(samoyedFamily[3].getAge() + " year(s) old,");
		System.out.println("a "+ samoyedFamily[3].getGender() + ", and is hypoallergenic: " + samoyedFamily[3].getHypoallergenic());
		
		System.out.println();
		
		samoyedFamily[0].bark(0+1);
		samoyedFamily[0].eat();
		
		System.out.println();
		
		samoyedFamily[1].run(5);		
		
		scan.close();
	}
}