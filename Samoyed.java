public class Samoyed
{
	private int age;
	private String gender;
	private boolean hypoallergenic;
	private int amountRunKM;
	
	//constructor class
	public Samoyed(int age, String gender, boolean hypoallergenic)
	{
		this.age = age;
		this.gender = gender;
		this.hypoallergenic = hypoallergenic;
	}
	
	//setters	
	public void setAge(int age)
	{
		this.age = age;
	}
	
	public void setGender(String gender)
	{
		this.gender = gender;
	}
	
	/*
	public void setHypoallergenic(boolean hypoallergenic)
	{
		this.hypoallergenic = hypoallergenic;
	}
	*/
	
	/* //I dont need this set method, because I just want to update it when a samoyed run()
	public void setAmountRunKM(int amountRunKM)
	{
		this.amountRunKM = amountRunKM;
	}
	*/
	
	//getters
	public int getAge()
	{
		return this.age;
	}
	
	public String getGender()
	{
		return this.gender;
	}
	
	public boolean getHypoallergenic()
	{
		return this.hypoallergenic;
	}
	
	public int getAmountRunKM()
	{
		return this.amountRunKM;
	}
	
	//instance methods
	public void bark(int n)
	{
		System.out.println("Woof! I am dog #" + n + " of the breed Samoyed and I'm a " + this.gender + ".");
		System.out.print("Woof! I ");
		if (this.hypoallergenic)
		{
			System.out.println("am hypoallergenic. You can hug me!");
		}
		else if (!this.hypoallergenic)
		{
			System.out.println("not hypoallergenic.");
		}
		else
		{
			System.out.println("don't know if I'm hypoallergenic or not.");
		}
	}
	
	public void eat()
	{
		int foodQty = this.age * 10;
		System.out.println("I am " + this.age + " year(s) old so I am going to eat " + foodQty + "g of food for supper!");
	}
	
	public void run(int numKM)
	{
		if (numIsPositive(numKM))
		{
			this.amountRunKM = numKM;
			System.out.println("I just ran " + numKM + "km for a total of " + this.amountRunKM + "km.");
		}
		else
		{
			System.out.println("The number should be positive, not negative.");
		}
	}
	
	//private helper method
	private boolean numIsPositive(int number)
	{
		return number >= 0;
	}
}